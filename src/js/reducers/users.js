import { Map } from "immutable";
import { ADD_NEW_USER, SET_USER_STATE, UPDATE_USER, MARK_CHECKED, SELECT_ALL, DELETE_SELECTED } from "../actions/users";

const initialState = Map({
    users: [],
    showUpdateForm: false,
});

const actionMap = {
    [ADD_NEW_USER]: (state, action) => {
        var prevStateUsers = Object.assign([], state.get('users'));
        var newUser = action.requestData;
        newUser.isSelected = false;
        prevStateUsers.push(newUser);
        return state.merge(Map({
            users: prevStateUsers,
        }));
    },
    [UPDATE_USER]: (state, action) => {
        var prevStateUsers = state.get('users');
        var user = action.requestData;
        var newStateUsers = [];
        prevStateUsers.map((o) => {
            var newO = null;
            if (o.id === user.id) {
                user.isSelected = false;
                newO = Object.assign({}, user);
            } else {
                newO = Object.assign({}, o);
            }
            if (newO) {
                newStateUsers.push(newO);
            }
        });
        return state.merge(Map({
            users: newStateUsers,
            showUpdateForm: false,
        }));
    },
    [MARK_CHECKED]: (state, action) => {
        var prevStateUsers = state.get('users');
        var user = action.requestData;
        var newStateUsers = [];
        prevStateUsers.map((o) => {
            var newO = null;
            if (o.id === user.id) {
                newO = Object.assign({}, user);
            } else {
                newO = Object.assign({}, o);
            }
            if (newO) {
                newStateUsers.push(newO);
            }
        });
        return state.merge(Map({
            users: newStateUsers,
        }));
    },
    [SELECT_ALL]: (state, action) => {
        var prevStateUsers = state.get('users');
        var flag = action.flag;
        var newStateUsers = [];
        prevStateUsers.map((o) => {
            var newO = Object.assign({}, o);
            newO.isSelected = flag;
            newStateUsers.push(newO);
        });
        return state.merge(Map({
            users: newStateUsers,
        }));
    },
    [DELETE_SELECTED]: (state, action) => {
        var prevStateUsers = state.get('users');
        var newStateUsers = [];
        prevStateUsers.map((o) => {
            if(!o.isSelected){
                newStateUsers.push(o);
            }
        });
        return state.merge(Map({
            users: newStateUsers,
        }));
    },
    [SET_USER_STATE]: (state, action) => {
        return state.merge(Map(action.stateData));
    },
};

export default function reducer(state = initialState, action = {}) {
    const fn = actionMap[action.type];
    return fn ? fn(state, action) : state;
}
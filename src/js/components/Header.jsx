import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FaPlusCircle } from 'react-icons/fa';
import SweetAlert from 'react-bootstrap-sweetalert';
import AddForm from './AddForm';
import { addNewUser } from '../actions/users';
import { ts } from '../helpers/funs';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showNewUserModal: false,
        }
    }

    render() {
        const { showNewUserModal } = this.state;
        return (
            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <span className="navbar-brand">User Profile Manager</span>
                    </div>
                    <div className="collapse navbar-collapse" id="myNavbar">
                        <ul className="nav navbar-nav navbar-right">
                            <li><a href="javascript:void(0)" onClick={this.handleOpenNewUserModal}><FaPlusCircle /> Add User</a></li>
                        </ul>
                    </div>
                </div>
                {showNewUserModal &&
                    <SweetAlert
                        showCancel={false}
                        showConfirm={false}
                        title="Add New User"
                        onConfirm={() => { }}
                        onCancel={() => { }}
                    >
                        <AddForm
                            onSubmit={this.handleNewUser}
                            onClose={this.handleCloseNewUserModal}
                        />
                    </SweetAlert>
                }
            </nav>
        );
    }

    handleOpenNewUserModal = () => {
        this.setState({ showNewUserModal: true });
    }

    handleCloseNewUserModal = () => {
        this.setState({ showNewUserModal: false });
    }

    handleNewUser = (data) => {
        const { dispatch } = this.props;
        dispatch(addNewUser(data));
        ts('Success');
        this.handleCloseNewUserModal();
    }
}

const mapStateToProps = (state) => {
    return {

    };
}

export default connect(
    mapStateToProps,
)(Header);
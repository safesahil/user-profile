import React, { Component } from 'react';
import { connect } from 'react-redux';
import { initialize } from "redux-form";
import { setUserState, markChecked, selectAllAction, deleteSelected } from '../actions/users';
import { FaTrash } from "react-icons/fa";
import { ts } from '../helpers/funs';

class SidePanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectAll: false,
        }
    }

    render() {
        const { usersList } = this.props;
        const { selectAll } = this.state;
        return (
            <div className="col-sm-2 sidenav">
                {usersList && usersList.length > 0 &&
                    <div>
                        <input type="checkbox" id="select_all" name="select_all" checked={selectAll} onClick={this.toggleAll} />
                        <button className="btn btn-danger" onClick={this.handleDelete}><FaTrash /></button>
                    </div>
                }
                {usersList && usersList.length > 0 &&
                    usersList.map((o, i) =>
                        <UserLink key={i} user={o} index={i} handleSelectUser={this.handleSelectUser} handleSelection={this.handleSelection} />
                    )
                }
                {(!usersList || usersList.length <= 0) &&
                    <span>No records found</span>
                }
            </div>
        );
    }

    handleSelectUser = (user) => {
        const { dispatch } = this.props;
        dispatch(initialize('update-user-form', user));
        let stateData = { showUpdateForm: true };
        dispatch(setUserState(stateData));
    }

    handleSelection = (user) => {
        const { dispatch } = this.props;
        user.isSelected = !user.isSelected;
        dispatch(markChecked(user));
    }

    toggleAll = () => {
        const { selectAll } = this.state;
        const { dispatch } = this.props;
        this.setState({ selectAll: !selectAll });
        dispatch(selectAllAction(!selectAll));
    }

    handleDelete = () => {
        const { selectAll } = this.state;
        const { dispatch } = this.props;
        dispatch(deleteSelected());
        ts('Deleted');
        this.setState({ selectAll: !selectAll });
    }
}

const mapStateToProps = (state) => {
    const { users } = state;
    return {
        usersList: users.get('users'),
    };
}

export default connect(
    mapStateToProps,
)(SidePanel);

class UserLink extends Component {
    render() {
        const { user, handleSelectUser, index, handleSelection } = this.props;
        return (
            <p>
                <input type="checkbox" id={`checkbox_${index}`} name={`checkbox_${index}`} checked={user.isSelected} onClick={() => handleSelection(user)} />
                <a href="javascript:void(0)" onClick={() => handleSelectUser(user)}>{(user.name) ? user.name : 'Name'}</a>
            </p>
        );
    }
}
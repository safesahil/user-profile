import React, { Component } from 'react';
import { connect } from 'react-redux';
import UpdateForm from './UpdateForm';
import { reset } from "redux-form";
import { setUserState, updateUser } from '../actions/users';
import { ts } from '../helpers/funs';

class Content extends Component {
    render() {
        const { showUpdateForm } = this.props;
        return (
            <div className="col-sm-10 text-left">
                <h1>Welcome</h1>
                {typeof showUpdateForm !== 'undefined' && showUpdateForm &&
                    <UpdateForm
                        onSubmit={this.handleUpdateUser}
                        onClose={this.handleReset}
                    />
                }
            </div>
        );
    }

    handleReset = () => {
        const { dispatch } = this.props;
        dispatch(reset('update-user-form'));
        let stateData = { showUpdateForm: false };
        dispatch(setUserState(stateData));
    }

    handleUpdateUser = (data) => {
        const { dispatch } = this.props;
        dispatch(updateUser(data));
        ts('Updated');
    }
}

const mapStateToProps = (state) => {
    const { users } = state;
    return {
        showUpdateForm: users.get('showUpdateForm'),
    };
}

export default connect(
    mapStateToProps,
)(Content);
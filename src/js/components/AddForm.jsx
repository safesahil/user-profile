import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from "redux-form";
import { InputField } from '../helpers/controls';
import { required } from '../helpers/validators';

class AddForm extends Component {
    render() {
        const { handleSubmit, onClose } = this.props;
        return (
            <form onSubmit={handleSubmit}>
                <Field
                    id="id"
                    name="id"
                    component={InputField}
                    wrapperClass="form-group"
                    className="form-control"
                    placeholder="Enter User Id"
                    errorClass="help-block"
                    validate={[required]}
                />
                <Field
                    id="name"
                    name="name"
                    component={InputField}
                    wrapperClass="form-group"
                    className="form-control"
                    placeholder="Enter User Name"
                    errorClass="help-block"
                    validate={[required]}
                />
                <div className="form-group">
                    <Field
                        id="description"
                        name="description"
                        component="textarea"
                        className="form-control"
                        placeholder="Enter User Details"
                    />
                </div>
                <button type="button" className="btn btn-danger mr-10" onClick={onClose}>Cancel</button>
                <button type="submit" className="btn btn-success">Save</button>
            </form>
        );
    }
}

AddForm = reduxForm({
    form: 'add-user-form',
})(AddForm);

const mapStateToProps = (state) => {
    return {

    };
}

export default connect(
    mapStateToProps,
)(AddForm);
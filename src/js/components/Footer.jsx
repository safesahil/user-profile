import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer className="container-fluid text-center">
                <p>Footer Text</p>
            </footer>
        );
    }
}

export default Footer;
export const ADD_NEW_USER = 'ADD_NEW_USER';
export const SET_USER_STATE = 'SET_USER_STATE';
export const UPDATE_USER = 'UPDATE_USER';
export const MARK_CHECKED = 'MARK_CHECKED';
export const SELECT_ALL = 'SELECT_ALL';
export const DELETE_SELECTED = 'DELETE_SELECTED';

export function addNewUser(requestData) {
    return {
        type: ADD_NEW_USER,
        requestData
    }
}

export function setUserState(stateData = {}) {
    return {
        type: SET_USER_STATE,
        stateData
    }
}

export function updateUser(requestData) {
    return {
        type: UPDATE_USER,
        requestData
    }
}

export function markChecked(requestData) {
    return {
        type: MARK_CHECKED,
        requestData
    }
}

export function selectAllAction(flag) {
    return {
        type: SELECT_ALL,
        flag
    }
}

export function deleteSelected() {
    return {
        type: DELETE_SELECTED,
    }
}
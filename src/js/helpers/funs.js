import React, { Component } from "react";
import { toast } from "react-toastify";
import { FaCheck } from 'react-icons/fa';

export function ts(msg = 'Success') {
    toast.success(
        <span>
            <FaCheck /> {msg}
        </span>
    );
}

export function te(msg = 'Something went wrong!') {
    toast.error(
        <span>
            {msg}
        </span>
    );
}
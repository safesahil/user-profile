import React, { Component } from 'react';
import Header from '../components/Header';
import '../../css/bootstrap.css';
import '../../css/style.css';
import SidePanel from '../components/SidePanel';
import Content from '../components/Content';
import Footer from '../components/Footer';
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

class App extends Component {
    render() {
        return (
            <div className="app-wrapper">
                <Header />
                <div className="container-fluid text-center">
                    <div className="row content">
                        <SidePanel />
                        <Content />
                    </div>
                </div>

                <Footer />

                <ToastContainer
                    position="top-right"
                    autoClose={3000}
                    hideProgressBar
                    newestOnTop
                    closeOnClick
                    rtl={false}
                    pauseOnVisibilityChange
                    draggable
                    pauseOnHover
                />

            </div>
        );
    }
}

export default App;